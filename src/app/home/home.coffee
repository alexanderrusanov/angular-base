appHomeModule = angular.module('app.home', [
  'ui.state',
  'titleService',
])

appHomeModule.config ($stateProvider)->
  $stateProvider.state 'home',
    url: '/home',
    views:
      "main":
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'


appHomeModule.controller 'HomeCtrl', ($scope, titleService)->
  titleService.setTitle('Home')
