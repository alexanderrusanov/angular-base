appModule = angular.module('app', [
  'templates-app',
  'templates-common',
  'app.home',
  'ui.state',
  'ui.route'
])

appModule.config ($stateProvider, $urlRouterProvider)->
  $urlRouterProvider.otherwise('/home')

appModule.run (titleService)->
  titleService.setSuffix(' | app')

appModule.controller 'AppCtrl', ($scope, $location)->
  null
